package com.example.basic24.myapplication;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_register;
    Button btn_unregister;
    MyReceiver myReceiver=new MyReceiver();
    public static final String action="action";
    public static final String text="text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_register=(Button) findViewById(R.id.btn_register);
        btn_unregister=(Button) findViewById(R.id.btn_unregister);

        btn_register.setOnClickListener(this);
        btn_unregister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
            {
                Log.wtf("TESTING" , "enter case");
                this.registerReceiver(myReceiver,new IntentFilter("android.intent.action.TIME_TICK"));

                break;
            }

            case R.id.btn_unregister:
            {
                this.unregisterReceiver(myReceiver);

                break
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
